<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    public function store(Request $request)
    {
        // Validation 
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        // Make Variable 
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        // Make Object User
        $user = new User([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password)
        ]);

        //Condition
        if ($user->save()) {
            $user->signin = [
                'href' => 'api/v1/user/signin',
                'method' => 'POST',
                'params' => 'email, password'
            ];
            $response = [
                'msg' => 'User Created',
                'user' => $user
            ];
            return response()->json($response, 201);
        }
        $response = [
            'msg' => 'An error occurred'
        ];
        return response()->json($response, 404);
    }

    public function signin(Request $request)
    {
        //
    }
}
